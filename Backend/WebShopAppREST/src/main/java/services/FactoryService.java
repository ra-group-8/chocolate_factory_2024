package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Chocolate;
import beans.Factory;
import dao.ChocolateDAO;
import dao.FactoryDAO;
import dao.UserDAO;

@Path("/factories")
public class FactoryService {
@Context
ServletContext ctx;

public FactoryService() {
}

@PostConstruct
public void init() {
if (ctx.getAttribute("chocolateDAO") == null) {
    String contextPath = ctx.getRealPath("");
ctx.setAttribute("chocolateDAO", new ChocolateDAO(contextPath));
}
if (ctx.getAttribute("factoryDAO") == null) {
    String contextPath = ctx.getRealPath("");
    //uriinfo mozda
ctx.setAttribute("factoryDAO", new FactoryDAO(contextPath, ctx));
}
}

@GET
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public Collection<Factory> getFactories() {
FactoryDAO dao = (FactoryDAO) ctx.getAttribute("factoryDAO");
return dao.findAll();
}

@POST
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public Factory newFactory(Factory factory) {
FactoryDAO dao = (FactoryDAO) ctx.getAttribute("factoryDAO");
return dao.save(factory);
}

@PUT
@Path("/{id}")
@Produces(MediaType.APPLICATION_JSON)
public Factory updateFactory(@PathParam("id") String id, Factory factory) {
FactoryDAO dao = (FactoryDAO) ctx.getAttribute("factoryDAO");
return dao.updateFactory(id, factory);
}


@DELETE
@Path("/{id}")
@Produces(MediaType.APPLICATION_JSON)
public Factory deleteFactory(@PathParam("id") String id) {
FactoryDAO dao = (FactoryDAO) ctx.getAttribute("factoryDAO");
return dao.deleteFactory(id);
}
}