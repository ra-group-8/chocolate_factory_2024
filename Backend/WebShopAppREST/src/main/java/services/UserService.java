package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.User;
import dao.CartDAO;
import dao.ChocolateDAO;
import dao.UserDAO;

@Path("/users")
public class UserService {
	@Context
	ServletContext ctx;
	
	public UserService() {
	}
	
	@PostConstruct
	public void init() {
	    String contextPath = ctx.getRealPath("");

	    if (ctx.getAttribute("chocolateDAO") == null) {
	        ctx.setAttribute("chocolateDAO", new ChocolateDAO(contextPath));
	        System.out.println("Initialized chocolateDAO");
	    }

	    if (ctx.getAttribute("cartDAO") == null) {
	        ctx.setAttribute("cartDAO", new CartDAO(contextPath, ctx));
	        System.out.println("Initialized cartDAO");
	    }

	    if (ctx.getAttribute("userDAO") == null) {
	        ctx.setAttribute("userDAO", new UserDAO(contextPath, ctx));
	        System.out.println("Initialized userDAO");
	    }
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> getUsers() {
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.findAll();
	}
	
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public User registerUser(User user) {
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.Register(user);
	}
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public User updateUser(@PathParam("id") String id, User user) {
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.updateUser(id, user);
	}
}
