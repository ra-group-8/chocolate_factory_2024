package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Cart;
import dao.CartDAO;
import dao.ChocolateDAO;
import dao.OrderDAO;

@Path("/carts")
public class CartService {
	@Context
	ServletContext ctx;

	public CartService() {
	}

	@PostConstruct
    public void init() {
    	// Ensure ChocolateDAO is initialized
        if (ctx.getAttribute("chocolateDAO") == null) {
            String contextPath = ctx.getRealPath("");
            ctx.setAttribute("chocolateDAO", new ChocolateDAO(contextPath));
        }

        // Now, initialize OrderDAO
        if (ctx.getAttribute("cartDAO") == null) {
            String contextPath = ctx.getRealPath("");
            ctx.setAttribute("cartDAO", new CartDAO(contextPath, ctx));
        }      
    }

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Cart> getCarts() {
	    CartDAO dao = (CartDAO) ctx.getAttribute("cartDAO");
	    return dao.findAll();
	}

	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Cart newCart(Cart cart) {
	    CartDAO dao = (CartDAO) ctx.getAttribute("cartDAO");
	    return dao.RegisterCart(cart);
	}

	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cart updateCart(@PathParam("id") String id, Cart cart) {
	    CartDAO dao = (CartDAO) ctx.getAttribute("cartDAO");
	    return dao.updateCart(id, cart);
	}

}
