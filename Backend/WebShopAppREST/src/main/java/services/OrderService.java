package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Order;
import dao.ChocolateDAO;
import dao.OrderDAO;

@Path("/orders")
public class OrderService {
    @Context
    ServletContext ctx;
    
    public OrderService() {
    }
    
    @PostConstruct
    public void init() {
    	// Ensure ChocolateDAO is initialized
        if (ctx.getAttribute("chocolateDAO") == null) {
            String contextPath = ctx.getRealPath("");
            ctx.setAttribute("chocolateDAO", new ChocolateDAO(contextPath));
        }

        // Now, initialize OrderDAO
        if (ctx.getAttribute("orderDAO") == null) {
            String contextPath = ctx.getRealPath("");
            ctx.setAttribute("orderDAO", new OrderDAO(contextPath, ctx));
        }
        
    }
    
    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Order> getOrders() {
        OrderDAO dao = (OrderDAO) ctx.getAttribute("orderDAO");
        return dao.findAll();
    }
    
    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Order registerOrder(Order order) {
        OrderDAO dao = (OrderDAO) ctx.getAttribute("orderDAO");
        return dao.Register(order);
    }
    
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Order updateOrder(@PathParam("id") String id, Order order) {
        OrderDAO dao = (OrderDAO) ctx.getAttribute("orderDAO");
        return dao.updateOrder(id, order);
    }
}

