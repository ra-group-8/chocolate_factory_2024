package services;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Base64;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.io.InputStream;
import java.io.ByteArrayInputStream;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Chocolate;
import beans.Factory;
import dao.ChocolateDAO;
import dao.FactoryDAO;


@Path("/uploadImage")
public class ImageService {
	
	@Context
	ServletContext ctx;
	
	public ImageService() {
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response uploadImage(Map<String, String> payload) {
	    try {
	        // Extract the base64-encoded image and file name from the payload
	        String base64Image = payload.get("imageData");
	        String fileName = payload.get("fileName");

	        // Decode the base64 string to a byte array
	        byte[] imageBytes = Base64.getDecoder().decode(base64Image);

	        // Convert the byte array to a BufferedImage
	        InputStream is = new ByteArrayInputStream(imageBytes);
	        BufferedImage image = ImageIO.read(is);

	        // Save the image
	        saveImage(image, fileName);

	        return Response.ok("Image uploaded successfully").build();
	    } catch (IOException e) {
	        e.printStackTrace();
	        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
	    }
	}

	public void saveImage(BufferedImage image, String fileName) throws IOException {
	    // Get the current working directory
	    String currentDir = System.getProperty("user.dir");

	    // Construct the relative path (this will be relative to the current working directory)
	    String outputPath = currentDir + "/Frontend/front-app/src/assets/" + fileName;

	    // Create a new file with the relative path
	    File outputfile = new File(outputPath);

	    // Print out the path for debugging
	    System.out.println("Saving image to: " + outputfile.getAbsolutePath());

	    // Write the image to the file
	    ImageIO.write(image, "png", outputfile);
	}
}
