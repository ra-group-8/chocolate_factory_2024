package beans;

import java.util.List;

public class Cart {
	private String id;
	private List<Chocolate> chocolates;
	private String user;
	private double totalCost;
	private List<String> quantities;
	
	public List<String> getQuantities() {
		return quantities;
	}

	public void setQuantities(List<String> quantities) {
		this.quantities = quantities;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Chocolate> getChocolates() {
		return chocolates;
	}

	public void setChocolates(List<Chocolate> chocolates) {
		this.chocolates = chocolates;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public Cart() {
		super();
	}

	public Cart(String id, List<Chocolate> chocolates, String user, double totalCost,List<String> quantities) {
		super();
		this.id = id;
		this.chocolates = chocolates;
		this.user = user;
		this.totalCost = totalCost;
		this.quantities = quantities;
	}
	
	
}
