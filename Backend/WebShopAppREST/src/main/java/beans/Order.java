package beans;

import java.time.LocalDateTime;
import java.util.List;

public class Order {
	private String identification;
	private List<Chocolate> chocolatesOrdered;
	private String factory;
	private LocalDateTime orderDateTime;
	private double price;
	private String customer;
	private String status;
	
	public Order() 
	{
			
	}
		
	public Order(String identification, List<Chocolate> chocolatesOrdered, String factory, LocalDateTime orderDateTime,
			double price, String customer, String status) {
		super();
		this.identification = identification;
		this.chocolatesOrdered = chocolatesOrdered;
		this.factory = factory;
		this.orderDateTime = orderDateTime;
		this.price = price;
		this.customer = customer;
		this.status = status;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	public List<Chocolate> getChocolatesOrdered() {
		return chocolatesOrdered;
	}
	public void setChocolatesOrdered(List<Chocolate> chocolatesOrdered) {
		this.chocolatesOrdered = chocolatesOrdered;
	}
	public String getFactory() {
		return factory;
	}
	public void setFactory(String factory) {
		this.factory = factory;
	}
	public LocalDateTime getOrderDateTime() {
		return orderDateTime;
	}
	public void setOrderDateTime(LocalDateTime orderDateTime) {
		this.orderDateTime = orderDateTime;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
	    // Use StringBuilder for better performance when concatenating strings
	    StringBuilder sb = new StringBuilder();
	    
	    sb.append("Order {")
	      .append("Identification: ").append(identification).append(", ")
	      .append("Chocolates Ordered: ").append(chocolatesOrdered).append(", ")
	      .append("Factory: ").append(factory).append(", ")
	      .append("Order Date Time: ").append(orderDateTime).append(", ")
	      .append("Price: ").append(price).append(", ")
	      .append("Customer: ").append(customer).append(", ")
	      .append("Status: ").append(status)
	      .append("}");
	    
	    return sb.toString();
	}
	
	
}
