package beans;

import java.util.Arrays;
import java.util.List;

public class Factory {
private String id;
private String name;
private List<Chocolate> availableChocolates;
private String operatingHours; //cuvati u formatu AA:BB-CC:DD
private String status; //radi ne radi trebalo bi enum
private String location;
private String logo;
private double score;
public Factory() {

}
public Factory(String id, String name, List<Chocolate> availableChocolates, String operatingHours, String status,
String location, double score, String logo) {
super();
this.id = id;
this.name = name;
this.availableChocolates = availableChocolates;
this.operatingHours = operatingHours;
this.status = status;
this.location = location;
this.score = score;
this.logo = logo;
}
public String getId() {
return id;
}
public void setId(String id) {
this.id = id;
}
public String getName() {
return name;
}
public void setName(String name) {
this.name = name;
}
public List<Chocolate> getAvailableChocolates() {
return availableChocolates;
}
public void setAvailableChocolates(List<Chocolate> availableChocolates) {
this.availableChocolates = availableChocolates;
}
public String getOperatingHours() {
return operatingHours;
}
public void setOperatingHours(String operatingHours) {
this.operatingHours = operatingHours;
}
public String getStatus() {
return status;
}
public void setStatus(String status) {
this.status = status;
}
public String getLocation() {
return location;
}
public void setLocation(String location) {
this.location = location;
}
public double getScore() {
return score;
}
public void setScore(double score) {
this.score = score;
}

public String getLogo() {
return logo;
}
public void setLogo(String logo) {
this.logo = logo;
}
@Override
public String toString() {
return "Factory [id=" + id + ", name=" + name + ", availableChocolatesId="
+ ", operatingHours=" + operatingHours + ", status=" + status
+ ", location=" + location + ", score=" + score + "]";
}
}
