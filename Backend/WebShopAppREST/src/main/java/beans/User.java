package beans;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class User {
	private String id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String gender;
	private LocalDate birthDate;
	private String userType;
	private String[] allOrders;
	private int cart;
	private int manages;
	private int points;
	private String customerType; //privremeno
	
	public User() {
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String[] getAllOrders() {
		return allOrders;
	}
	public void setAllOrders(String[] allOrders) {
		this.allOrders = allOrders;
	}
	public int getCart() {
		return cart;
	}
	public void setCart(int cart) {
		this.cart = cart;
	}
	public int getManages() {
		return manages;
	}
	public void setManages(int manages) {
		this.manages = manages;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public User(String id, String username, String password, String firstName, String lastName, String gender,
			LocalDate birthDate, String userType, String[] allOrders, int cart, int manages, int points,
			String customerType) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthDate = birthDate;
		this.userType = userType;
		this.allOrders = allOrders;
		this.cart = cart;
		this.manages = manages;
		this.points = points;
		this.customerType = customerType;
	}
	
	
}
