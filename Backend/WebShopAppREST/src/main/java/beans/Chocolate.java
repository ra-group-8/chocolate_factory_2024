package beans;

public class Chocolate {
	private String id;
	private String name;
	private double price;
	private String variety; //za kuvanje,obicna...
	private String factoryId;
	private String type; //crna bela mlecna
	private int weight; //in grams
	private String description;
	private String logo;
	private String status; //InStock OutOfStock
	private int quantity;
	
	
	
	
	
	
	public Chocolate() {
		
	}
	public Chocolate(String id, String name, double price, String variety, String factoryId, String type, int weight,
			String description, String status, int quantity, String logo) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.variety = variety;
		this.factoryId = factoryId;
		this.type = type;
		this.weight = weight;
		this.description = description;
		this.status = status;
		this.quantity = quantity;
		this.logo = logo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getVariety() {
		return variety;
	}
	public void setVariety(String variety) {
		this.variety = variety;
	}
	public String getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	@Override
	public String toString() {
	    return "Chocolate {"
	           + "ID: " + id + ", "
	           + "Name: " + name + ", "
	           + "Quantity: " + quantity + ", "
	           + "Price: " + price
	           + "}";
	}
	
}
