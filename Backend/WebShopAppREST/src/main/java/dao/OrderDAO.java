package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import beans.Chocolate;
import beans.Factory;
import beans.Order;
import beans.User;

public class OrderDAO {
	private HashMap<String, Order> orders = new HashMap<String, Order>();
	private String contextPath;
	private ChocolateDAO chocolateDAO;
	
	public OrderDAO() {
		
	}
	
	public OrderDAO(String contextPath, ServletContext ctx) {
		this.contextPath = contextPath;
	    this.chocolateDAO = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
	    if (this.chocolateDAO == null) {
	        throw new IllegalStateException("ChocolateDAO is not initialized in the context.");
	    }
	    loadOrders(contextPath);
	}
	
	public Collection<Order> findAll() {
		
		return orders.values();
	}

	public Order findOrder(String id) {
		return orders.containsKey(id) ? orders.get(id) : null;
	}
	
	public Order updateOrder(String id, Order order) {
	    // Check if the order with the given ID exists in the map
	    if (orders.containsKey(id)) {
	        order.setIdentification(id); // Ensure the ID matches the one from the URL
	        orders.put(id, order); // Update the order in the map
	        
	        // Update orders.txt file
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
	        
	        // List to hold all orders
	        List<Order> allOrders = new ArrayList<>(orders.values());
	        
	        try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "orders.txt"))) {
	            for (Order ord : allOrders) {
	                String chocolateIds = ord.getChocolatesOrdered().stream()
	                                         .map(Chocolate::getId)
	                                         .collect(Collectors.joining(","));
	                String quantities = ord.getChocolatesOrdered().stream()
	                    .map(Chocolate::getQuantity) // Assuming Chocolate has a getQuantity() method
	                    .map(String::valueOf) // Convert Integer to String
	                    .collect(Collectors.joining(","));
	                
	                out.write(String.format("%s;%s;%s;%s;%s;%s;%s;%s",
	                    ord.getIdentification(),           // Order ID
	                    chocolateIds,                        // Comma-separated list of chocolate IDs
	                    ord.getFactory(),        			//Name of a Factory
	                    ord.getOrderDateTime().format(formatter), // Format LocalDateTime as dd.MM.yyyy HH:mm:ss
	                    String.format("%.2f", ord.getPrice()),                    // Order price
	                    ord.getCustomer(),   				//Username of a Customer
	                    ord.getStatus(),                    // Order status
	                    quantities
	                ));
	                out.newLine(); // Add a new line after each order's data
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	        return order;
	    } else {
	        // Handle the case where the order with the given ID does not exist
	        return null;
	    }
	}


	
	public Order Register(Order order) {
	    // Find the max ID and increment it
	    Integer maxId = orders.keySet().stream()
	                          .mapToInt(Integer::parseInt)
	                          .max()
	                          .orElse(-1);
	    maxId++;
	    order.setIdentification(maxId.toString());
	    orders.put(order.getIdentification(), order);

	    // DateTimeFormatter for formatting the date and time
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

	    // Join the list of chocolates into a comma-separated string of chocolate IDs
	    String chocolateIds = order.getChocolatesOrdered().stream()
	                               .map(Chocolate::getId) // Assuming Chocolate has a getId() method
	                               .collect(Collectors.joining(","));
	    String quantities = order.getChocolatesOrdered().stream()
			                .map(Chocolate::getQuantity) // Assuming Chocolate has a getQuantity() method
			                .map(String::valueOf) // Convert Integer to String
			                .collect(Collectors.joining(","));

	    // Write the new order to the orders.txt file
	    try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "orders.txt", true))) {
	        // Append to the file instead of overwriting it
	        out.write(String.format("%s;%s;%s;%s;%s;%s;%s;%s",
	                order.getIdentification(),           // Order ID
	                chocolateIds,                        // Comma-separated list of chocolate IDs
	                order.getFactory(),        			//Name of a Factory
	                order.getOrderDateTime().format(formatter), // Format LocalDateTime as dd.MM.yyyy HH:mm:ss
	                String.format("%.2f", order.getPrice()),                    // Order price
	                order.getCustomer(),   				//Username of a Customer
	                order.getStatus(),                    // Order status
	                quantities
	                
	        ));
	        out.newLine();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    return order;
	}

	
	private void loadOrders(String contextPath) {
	    // Define the formatter to match the date-time format in the file
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

	    try (BufferedReader in = new BufferedReader(new FileReader(new File(contextPath + "/orders.txt")))) {
	        String line;
	        while ((line = in.readLine()) != null) {
	            line = line.trim();
	            if (line.isEmpty() || line.startsWith("#")) continue;

	            String[] tokens = line.split(";");
	            if (tokens.length < 8) {  // Update this number based on Order fields count
	                System.err.println("Incorrect number of fields: " + line);
	                continue;
	            }

	            // Extract fields
	            String id = getOrDefault(tokens, 0, "");
	            List<Chocolate> chocolatesOrdered = findChocolatesByIds(getOrDefault(tokens, 1, "").split(","));
	            LocalDateTime orderDateTime = parseDateTime(getOrDefault(tokens, 3, "").trim(), formatter);
	            double price = parseDouble(getOrDefault(tokens, 4, "0.0"));
	            String customer = getOrDefault(tokens, 5, "");
	            String status = getOrDefault(tokens, 6, "");
	            
	            // Update chocolate quantities
	            updateChocolateQuantities(chocolatesOrdered, getOrDefault(tokens, 7, ""));

	            // Add order to the map
	            orders.put(id, new Order(id, chocolatesOrdered, getOrDefault(tokens, 2, ""), orderDateTime, price, customer, status));
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	private LocalDateTime parseDateTime(String dateTimeStr, DateTimeFormatter formatter) {
	    try {
	        return LocalDateTime.parse(dateTimeStr, formatter);
	    } catch (DateTimeParseException e) {
	        System.err.println("Invalid date-time format: " + dateTimeStr);
	        return null;  // Handle null value appropriately in calling method
	    }
	}

	private double parseDouble(String numberStr) {
	    try {
	        return Double.parseDouble(numberStr);
	    } catch (NumberFormatException e) {
	        System.err.println("Invalid number format: " + numberStr);
	        return 0.0;  // Default value for invalid number format
	    }
	}

	private void updateChocolateQuantities(List<Chocolate> chocolates, String quantityList) {
	    String[] quantitiesStr = quantityList.split(",");
	    for (int i = 0; i < chocolates.size() && i < quantitiesStr.length; i++) {
	        try {
	            int quantity = Integer.parseInt(quantitiesStr[i].trim());
	            chocolates.get(i).setQuantity(quantity);
	        } catch (NumberFormatException e) {
	            System.err.println("Invalid quantity format: " + quantitiesStr[i]);
	        }
	    }
	}

	// Helper method to get value or default
	private String getOrDefault(String[] tokens, int index, String defaultValue) {
	    return index < tokens.length ? tokens[index].trim() : defaultValue;
	}

	// Method to find chocolates by their IDs
	private List<Chocolate> findChocolatesByIds(String[] chocolateIds) {
	    List<Chocolate> chocolates = new ArrayList<>();
	    for (String id : chocolateIds) {
	        Chocolate chocolate = chocolateDAO.findChocolate(id);
	        if (chocolate != null) {
	            chocolates.add(chocolate);
	        }
	    }
	    return chocolates;
	}



}
