package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import java.time.format.DateTimeFormatter;

import beans.Cart;
import beans.User;

public class UserDAO {
	private HashMap<String, User> users = new HashMap<String, User>();
	private String contextPath;
	private CartDAO cartDAO;
	private ChocolateDAO chocolateDAO;
	
	public UserDAO() {
		
	}
	
	public UserDAO(String contextPath, ServletContext ctx) {
		this.contextPath = contextPath;
		this.chocolateDAO = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
		this.cartDAO = (CartDAO) ctx.getAttribute("cartDAO");
		System.out.println(this.cartDAO);
        loadUsers(contextPath);
	}

	public Collection<User> findAll() {
		
		return users.values();
	}

	public User findUser(String id) {
		return users.containsKey(id) ? users.get(id) : null;
	}
	
	public User updateUser(String id, User user) {
	    if (users.containsKey(id)) {
	        user.setId(id); // Ensure the ID matches the one from the URL
	        users.put(id, user); // Update the chocolate in the map

	        // Update chocolates.txt file
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	        try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "users.txt"))) {
	            for (User usr : users.values()) {
	                out.write(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%d;%d;%d;%s;",
	                	usr.getId(),
	                    usr.getUsername(),
	                    usr.getPassword(),
	                    usr.getFirstName(),
	                    usr.getLastName(),
	                    usr.getGender(),
	                    usr.getBirthDate().format(formatter), // Format LocalDate as dd.MM.yyyy
	                    usr.getUserType(),
	                    String.join(",", usr.getAllOrders()), // Join the array of orders into a comma-separated string
	                    usr.getCart(),
	                    usr.getManages(),
	                    usr.getPoints(),
	                    usr.getCustomerType()
	                ));
	                out.newLine(); // Add a new line after each user's data
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	        return user;
	    } else {
	        // Handle the case where the chocolate with the given ID does not exist
	        return null;
	    }
	}
	
	public User Register(User user) {
	    Integer maxId = -1;
	    for (String id : users.keySet()) {
	        int idNum = Integer.parseInt(id);
	        if (idNum > maxId) {
	            maxId = idNum;
	        }
	    }
	    maxId++;
	    user.setId(maxId.toString());
	    if (user.getUserType().equals("Customer")) {
	        // Create a new Cart object and pass necessary details
	        Cart cart = new Cart();
	        cart.setId(user.getId());
	        cart.setChocolates(new ArrayList<>()); // No chocolates initially
	        cart.setUser(user.getId()); // Associate the cart with the user
	        cart.setTotalCost(0.0); // Initial total cost

	        // Register the cart in CartDAO
	        cartDAO.RegisterCart(cart);

	        // Set the cart ID to the user
	        user.setCart(Integer.parseInt(cart.getId()));
	    } else {
	        user.setCart(0);
	    }

	    users.put(user.getId(), user);
	    
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	    
	    String allOrders = Arrays.stream(user.getAllOrders())
		        .filter(Objects::nonNull)
		        .collect(Collectors.joining(","));
		    
		    // Check if the result is empty and replace it with a single space if true
	    		allOrders = allOrders.isEmpty() ? " " : allOrders;
	    
	    try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "users.txt", true))) {
	        // Append to the file instead of overwriting it
	        out.write(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%d;%d;%d;%s\n",
	                user.getId(),
	                user.getUsername(),
	                user.getPassword(),
	                user.getFirstName(),
	                user.getLastName(),
	                user.getGender(),
	                user.getBirthDate().format(formatter),
	                user.getUserType(),
	                allOrders,
	                user.getCart(),
	                user.getManages(),
	                user.getPoints(),
	                user.getCustomerType()));
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return user;
	}
	
	private void loadUsers(String contextPath) {
	    try (BufferedReader in = new BufferedReader(new FileReader(new File(contextPath + "/users.txt")))) {
	        String line;
	        while ((line = in.readLine()) != null) {
	            line = line.trim();
	            if (line.isEmpty() || line.startsWith("#")) continue;

	            String[] tokens = line.split(";");
	            if (tokens.length < 12) {
	                System.err.println("Incorrect number of fields: " + line);
	                continue;
	            }

	            // Extract and trim fields
	            String id = getOrDefault(tokens, 0, "");
	            String username = getOrDefault(tokens, 1, "");
	            String password = getOrDefault(tokens, 2, "");
	            String firstName = getOrDefault(tokens, 3, "");
	            String lastName = getOrDefault(tokens, 4, "");
	            String gender = getOrDefault(tokens, 5, "");
	            String birthDate = getOrDefault(tokens, 6, "");
	            String userType = getOrDefault(tokens, 7, "");
	            String allOrdersStr = getOrDefault(tokens, 8, " ");
	            String cart = getOrDefault(tokens, 9, "0");
	            String manages = getOrDefault(tokens, 10, "0");
	            String points = getOrDefault(tokens, 11, "0");
	            String customerType = getOrDefault(tokens, 12, "");

	            // Parse fields
	            String[] allOrders = allOrdersStr.isEmpty() ? new String[0] : allOrdersStr.split(",");
	            LocalDate parsedBirthDate = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd.MM.yyyy"));

	            int parsedPoints = parseIntOrDefault(points, 0);

	            // Add user to map
	            users.put(id, new User(
	                    id, 
	                    username, 
	                    password, 
	                    firstName, 
	                    lastName, 
	                    gender, 
	                    parsedBirthDate, 
	                    userType, 
	                    allOrders, 
	                    Integer.parseInt(cart), 
	                    Integer.parseInt(manages), 
	                    parsedPoints, 
	                    customerType
	            ));
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	private String getOrDefault(String[] tokens, int index, String defaultValue) {
	    return index < tokens.length ? tokens[index].trim() : defaultValue;
	}

	private int parseIntOrDefault(String value, int defaultValue) {
	    try {
	        return Integer.parseInt(value.trim());
	    } catch (NumberFormatException e) {
	        return defaultValue;
	    }
	}
}
