package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import beans.Chocolate;
import beans.Factory;
import beans.Product;

public class FactoryDAO {
	private HashMap<String, Factory> factories = new HashMap<String, Factory>();
	private String contextPath;
	private ChocolateDAO chocolateDAO;
	
	public FactoryDAO() {
		
	}
	
	/***
	 * @param contextPath Putanja do aplikacije u Tomcatu. Moze se pristupiti samo iz servleta.
	 */
	public FactoryDAO(String contextPath, ServletContext ctx) {
		this.contextPath = contextPath;
		this.chocolateDAO = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
        if (this.chocolateDAO == null) {
            throw new IllegalStateException("ChocolateDAO is not initialized in the context.");
        }
        loadFactories(contextPath);
	}

	public Collection<Factory> findAll() {
		return factories.values();
	}

	public Factory findFactory(String id) {
		return factories.containsKey(id) ? factories.get(id) : null;
	}
	
	public Factory updateFactory(String id, Factory factory) {
	    if (factories.containsKey(id)) {
	        factory.setId(id); // Ensure the ID matches the one from the URL
	        factories.put(id, factory); // Update the factory in the map

	        // Update factories.txt file
	        try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "factories.txt"))) {
	            for (Factory fac : factories.values()) {
	                String chocolateIds = fac.getAvailableChocolates().isEmpty() ? " " : 
	                    fac.getAvailableChocolates().stream()
	                        .map(Chocolate::getId)
	                        .collect(Collectors.joining(","));
	                
	                out.write(String.format("%s;%s;%s;%s;%s;%s;%.2f;%s",
	                        fac.getId(),
	                        fac.getName(),
	                        chocolateIds,
	                        fac.getOperatingHours(),
	                        fac.getStatus(),
	                        fac.getLocation(),
	                        fac.getScore(),
	                        fac.getLogo()
	                ));
	                out.newLine();
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	        return factory;
	    } else {
	        // Handle the case where the factory with the given ID does not exist
	        return null;
	    }
	}

	public Factory save(Factory factory) {
		Integer maxId = -1;
		for (String id : factories.keySet()) {
			int idNum =Integer.parseInt(id);
			if (idNum > maxId) {
				maxId = idNum;
			}
		}
		maxId++;
		factory.setId(maxId.toString());
		factories.put(factory.getId(), factory);
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(contextPath, "factories.txt"), StandardOpenOption.APPEND)) {
			String availableChocolates = factory.getAvailableChocolates().stream()
				    .filter(Objects::nonNull) // Filter out any null objects
				    .map(Chocolate::getId)    // Extract the 'id' from each Chocolate object
				    .collect(Collectors.joining(",")); // Join the IDs into a single string, separated by commas

		    
		    // Check if the result is empty and replace it with a single space if true
		    availableChocolates = availableChocolates.isEmpty() ? " " : availableChocolates;
		    
		    writer.write(String.format("%s;%s;%s;%s;%s;%s;%.2f;%s%n", 
		        factory.getId(), 
		        factory.getName(), 
		        availableChocolates, 
		        factory.getOperatingHours(), 
		        factory.getStatus(), 
		        factory.getLocation(), 
		        factory.getScore(),
		    	factory.getLogo()));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return factory;
	}
	
	private void loadFactories(String contextPath) {
	    BufferedReader in = null;
	    try {
	    	File file = new File(contextPath + "/factories.txt");
	    	in = new BufferedReader(new FileReader(file));
	    	String line;
	    	String id = "", name = "", operatingHours = "", status = "", location = "", score = "", logo = "";
	    	List<Chocolate> availableChocolates = new ArrayList<>();
	    	StringTokenizer st;

	    	while ((line = in.readLine()) != null) {
	    	    line = line.trim();
	    	    if (line.equals("") || line.indexOf('#') == 0)
	    	        continue;

	    	    st = new StringTokenizer(line, ";");
	    	    while (st.hasMoreTokens()) {
	    	        id = st.nextToken().trim();
	    	        name = st.nextToken().trim();
	    	        String[] availableChocolatesId = st.nextToken().trim().split(",");  // Split by comma
	    	        
	    	        // Convert availableChocolatesId to a List<Chocolate>
	    	        availableChocolates = Arrays.stream(availableChocolatesId)
	    	                                    .map(chocolateId -> chocolateDAO.findChocolate(chocolateId)) // Replace with your method to fetch Chocolate by ID
	    	                                    .filter(Objects::nonNull)
	    	                                    .collect(Collectors.toList());
                
	    	        operatingHours = st.nextToken().trim();
	    	        status = st.nextToken().trim();
	    	        location = st.nextToken().trim();
	    	        score = st.nextToken().trim();
	    	        logo = st.nextToken().trim();
	    	    }

	    	    // Create the Factory object with a List<Chocolate> for availableChocolates
	    	    factories.put(id, new Factory(id, name, availableChocolates, operatingHours, status, location, Double.parseDouble(score), logo));
	    	
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (in != null) {
	            try {
	                in.close();
	            } catch (Exception e) {
	                // Ignore exception
	            }
	        }
	    }
	}
	public Factory deleteFactory(String id) {
		Factory removedFactory = factories.remove(id);
	    if (removedFactory != null) {
	        try {
	            Path filePath = Paths.get(contextPath, "factories.txt");
	            List<String> lines = Files.readAllLines(filePath);
	            lines.removeIf(line -> line.startsWith(id + ";")); // Remove the line with matching ID
	            Files.write(filePath, lines);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return removedFactory;
	}
//	public Collection<Product> findByNameAndPriceRange(String name, 
//			double minPrice, 
//			double maxPrice) {
//		return products.values()
//		.stream()
//		.filter(x -> x.getName().contains(name) 
//		&& x.getPrice() <= maxPrice 
//		&& x.getPrice() >= minPrice)
//		.collect(Collectors.toList());
//	}
}