package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import beans.Chocolate;

public class ChocolateDAO {
	private HashMap<String, Chocolate> chocolates = new HashMap<String, Chocolate>();
	private String contextPath;
	
	public ChocolateDAO() {
		
	}
	
	public ChocolateDAO(String contextPath) {
		this.contextPath = contextPath;
        loadChocolates(contextPath);
	}

	public Collection<Chocolate> findAll() {
		return chocolates.values();
	}

	public Chocolate findChocolate(String id) {
		return chocolates.containsKey(id) ? chocolates.get(id) : null;
	}
	
	public Chocolate updateChocolate(String id, Chocolate chocolate) {
	    if (chocolates.containsKey(id)) {
	        chocolate.setId(id); // Ensure the ID matches the one from the URL
	        chocolates.put(id, chocolate); // Update the chocolate in the map

	        // Update chocolates.txt file
	        try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "chocolates.txt"))) {
	            for (Chocolate choco : chocolates.values()) {
	                out.write(String.format("%s;%s;%.2f;%s;%s;%s;%d;%s;%s;%d;%s%n",
	                        choco.getId(),
	                        choco.getName(),
	                        choco.getPrice(),
	                        choco.getVariety(),
	                        choco.getFactoryId(),
	                        choco.getType(),
	                        choco.getWeight(),
	                        choco.getDescription(),
	                        choco.getStatus(),
	                        choco.getQuantity(),
	                        choco.getLogo()));
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	        return chocolate;
	    } else {
	        // Handle the case where the chocolate with the given ID does not exist
	        return null;
	    }
	}
	
	public Chocolate save(Chocolate chocolate) {
	    Integer maxId = -1;
	    for (String id : chocolates.keySet()) {
	        int idNum = Integer.parseInt(id);
	        if (idNum > maxId) {
	            maxId = idNum;
	        }
	    }
	    maxId++;
	    chocolate.setId(maxId.toString());
	    chocolates.put(chocolate.getId(), chocolate);
	    
	    try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "chocolates.txt", true))) {
	        // Append to the file instead of overwriting it
	        out.write(String.format("%s;%s;%.2f;%s;%s;%s;%d;%s;%s;%d;%s%n",
	                chocolate.getId(),
	                chocolate.getName(),
	                chocolate.getPrice(),
	                chocolate.getVariety(),
	                chocolate.getFactoryId(),
	                chocolate.getType(),
	                chocolate.getWeight(),
	                chocolate.getDescription(),
	                chocolate.getStatus(),
	                chocolate.getQuantity(),
	                chocolate.getLogo()));
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return chocolate;
	}
	
	private void loadChocolates(String contextPath) {
		BufferedReader in = null;
		try {
			File file = new File(contextPath + "/chocolates.txt");
			in = new BufferedReader(new FileReader(file));
			String line, id = "", name = "", price = "",variety = "",factoryId = "",type = "",weight = "",description = "",status = "",quantity = "",logo="";
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					name = st.nextToken().trim();
					price = st.nextToken().trim();
					variety = st.nextToken().trim();
					factoryId = st.nextToken().trim();
					type = st.nextToken().trim();
					weight = st.nextToken().trim();
					description = st.nextToken().trim();
					status = st.nextToken().trim();
					quantity = st.nextToken().trim();
					logo = st.nextToken().trim();
				}
				chocolates.put(id, new Chocolate(id, name,Double.parseDouble(price),variety,factoryId,type, Integer.parseInt(weight),
						description,status,Integer.parseInt(quantity), logo));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ( in != null ) {
				try {
					in.close();
				}
				catch (Exception e) { }
			}
		}
		
	}
	public Chocolate deleteChocolate(String id) {
		Chocolate removedChocolate = chocolates.remove(id);
	    if (removedChocolate != null) {
	        try {
	            Path filePath = Paths.get(contextPath, "chocolates.txt");
	            List<String> lines = Files.readAllLines(filePath);
	            lines.removeIf(line -> line.startsWith(id + ";")); // Remove the line with matching ID
	            Files.write(filePath, lines);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return removedChocolate;
	}
}
