package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import beans.Cart;
import beans.Chocolate;

public class CartDAO {
	private HashMap<String, Cart> carts = new HashMap<String, Cart>();
	private String contextPath;
	private ChocolateDAO chocolateDAO;
	
	public CartDAO() {
		
	}
	
	public CartDAO(String contextPath, ServletContext ctx) {
		this.contextPath = contextPath;
	    this.chocolateDAO = (ChocolateDAO) ctx.getAttribute("chocolateDAO");
	    if (this.chocolateDAO == null) {
	        throw new IllegalStateException("ChocolateDAO is not initialized in the context.");
	    }
	    loadCarts(contextPath);
	}
	
	public Collection<Cart> findAll() {
		
		return carts.values();
	}
	
	public Cart findCart(String id) {
		return carts.containsKey(id) ? carts.get(id) : null;
	}
	
	public Cart updateCart(String id, Cart cart) {
	    if (carts.containsKey(id)) {
	        cart.setId(id); // Ensure the ID matches the one from the URL
	        carts.put(id, cart); // Update the cart in the map

	        try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "carts.txt"))) {
	            for (Cart crt : carts.values()) {
	                String chocolateIds = crt.getChocolates().stream()
	                                         .map(Chocolate::getId)
	                                         .collect(Collectors.joining(","));
	                
	                // Join the list of quantities into a comma-separated string, filter out empty entries
	                String quantities = crt.getQuantities().stream()
	                                        .filter(q -> !q.trim().isEmpty())
	                                        .collect(Collectors.joining(","));

	                out.write(String.format("%s;%s;%s;%s;%s\n",
	                    crt.getId(),
	                    crt.getUser(),
	                    chocolateIds.isEmpty() ? " " : chocolateIds,	                    
	                    quantities.isEmpty() ? " " : quantities,
	                    crt.getTotalCost()));
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	        return cart;
	    } else {
	        // Handle the case where the cart with the given ID does not exist
	        return null;
	    }
	}



	public Cart RegisterCart(Cart cart) {
	    // Join the list of chocolates into a comma-separated string of chocolate IDs
	    String chocolateIds = cart.getChocolates() != null ? 
	                          cart.getChocolates().stream()
	                              .map(Chocolate::getId)
	                              .collect(Collectors.joining(",")) : " ";

	    // Join the list of quantities into a comma-separated string, filter out empty entries
	    String quantities = cart.getQuantities() != null ? 
	                        cart.getQuantities().stream()
	                            .filter(q -> !q.trim().isEmpty())
	                            .collect(Collectors.joining(",")) : " ";

	    try (BufferedWriter out = new BufferedWriter(new FileWriter(contextPath + "carts.txt", true))) {
	        // Append to the file instead of overwriting it
	        out.write(String.format("%s;%s;%s;%s;%s\n",
	                cart.getId(),
	                cart.getUser(),
	                chocolateIds.isEmpty() ? " " : chocolateIds,           
	                quantities.isEmpty() ? " " : quantities,
	                cart.getTotalCost()));
	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	    return cart;
	}




	private void loadCarts(String contextPath) {
	    try (BufferedReader in = new BufferedReader(new FileReader(new File(contextPath + "/carts.txt")))) {
	        String line;
	        while ((line = in.readLine()) != null) {
	            line = line.trim();
	            if (line.isEmpty() || line.startsWith("#")) continue;

	            String[] tokens = line.split(";");
	            if (tokens.length != 5) {
	                System.err.println("Incorrect number of fields: " + line);
	                continue;
	            }

	            // Extract fields
	            String id = getOrDefault(tokens, 0, "").trim();
	            String user = getOrDefault(tokens, 1, "").trim();
	            List<Chocolate> chocolatesOrdered = findChocolatesByIds(getOrDefault(tokens, 2, "").split(","));
	            List<String> quantities = Arrays.stream(getOrDefault(tokens, 3, "").split(","))
                        .filter(q -> !q.trim().isEmpty())
                        .collect(Collectors.toList());
	            double totalCost = parseDouble(getOrDefault(tokens, 4, "0.0").trim());

	            // Add cart to the map
	            carts.put(id, new Cart(id, chocolatesOrdered, user, totalCost, quantities));
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}


	
	
	private double parseDouble(String numberStr) {
	    try {
	        return Double.parseDouble(numberStr);
	    } catch (NumberFormatException e) {
	        System.err.println("Invalid number format: " + numberStr);
	        return 0.0;  // Default value for invalid number format
	    }
	}
	
	private String getOrDefault(String[] tokens, int index, String defaultValue) {
	    return index < tokens.length ? tokens[index].trim() : defaultValue;
	}
	
	private List<Chocolate> findChocolatesByIds(String[] chocolateIds) {
	    List<Chocolate> chocolates = new ArrayList<>();
	    for (String id : chocolateIds) {
	        Chocolate chocolate = chocolateDAO.findChocolate(id);
	        if (chocolate != null) {
	            chocolates.add(chocolate);
	        }
	    }
	    return chocolates;
	}

	
}
