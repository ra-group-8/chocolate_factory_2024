import { createRouter, createWebHashHistory } from 'vue-router';
import LoginView from '@/views/LoginView.vue';
import ChocolateView from '../views/ChocolateView.vue';
import SignUpView from '../views/SignUpView.vue';
import ProfileView from '@/views/ProfileView.vue';
import FactoryOverview from '@/views/FactoryView.vue';
import EditProfileView from '@/views/EditProfileView.vue';

const routes = [
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },
  {
    path: "/",
    name: "chocolate",
    component: ChocolateView,
  },
  {
    path: "/signup",
    name: "signup",
    component: SignUpView,
  },
  {
    path: '/profile', 
    name: 'Profile', 
    component: ProfileView,
  },  
  {
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/factory", // New route for factory overview
    name: "factoryOverview",
    component: FactoryOverview,
  },
  {
    path: "/edit-profile",
    name: "editProfile",
    component: EditProfileView,
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
